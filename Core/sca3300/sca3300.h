/*
 * sca3300.h
 *
 *  Created on: Oct 26, 2021
 *      Author: Ajmi
 */

#ifndef SRC_SCA3300_SCA3300_H_
#define SRC_SCA3300_SCA3300_H_

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include "stm32l0xx_hal.h"

#define SCA3300_CHIP_ID           0x0051
#define TRUE						1
#define FALSE 						0

#define TEMP_SIGNAL_SENSITIVITY    18.9
#define TEMP_ABSOLUTE_ZERO       273.15

/* SCA3300 Sensivity definitions */
#define SENSITIVITY_MODE_1          2700 // +/-6g   1350 LSB/g
#define SENSITIVITY_MODE_2          1350 // +/-2g   2700 LSB/g
#define SENSITIVITY_MODE_3_4        5400 // +/-1.5g 5400 LSB/g

#define REQ_READ_ACC_X        0x040000F7
#define REQ_READ_ACC_Y        0x080000FD
#define REQ_READ_ACC_Z        0x0C0000FB
#define REQ_READ_STO          0x100000E9
#define REQ_READ_TEMP         0x140000EF
#define REQ_READ_STATUS       0x180000E5
#define REQ_WRITE_SW_RESET    0xB4002098
#define REQ_WRITE_MODE1       0xB400001F
#define REQ_WRITE_MODE2       0xB4000102
#define REQ_WRITE_MODE3       0xB4000225
#define REQ_WRITE_MODE4       0xB4000338
#define REQ_READ_WHOAMI       0x40000091

/* SCA3300 SPI frame field masks */
#define OPCODE_FIELD_MASK     0xFC000000
#define RS_FIELD_MASK         0x03000000
#define DATA_FIELD_MASK       0x00FFFF00
#define CRC_FIELD_MASK        0x000000FF

/* SCA3300 return status */
#define ST_START_UP                 0x00
#define ST_NORMAL_OP                0x01
#define ST_ERROR                    0x11


typedef struct sca3300Frame
{
  uint16_t st_Data;        /**< Data */
  uint16_t st_Crc;        /**< Cyclic Redundancy Check */
  uint8_t st_ReturnStatus; /**< Return Status Code*/
  uint8_t st_IsValid;    /**< Frame is valid? */
}sca3300Frame;

enum accelAxe
{
  ACCEL_X = 0,
  ACCEL_Y,
  ACCEL_Z,
};

enum operationMode
{
  ERR = 0,
  OPMODE1, /*!<   3g full-scale. 88 Hz 1st order low pass filter (default) */
  OPMODE2, /*!<   6g full-scale. 88 Hz 1st order low pass filter */
  OPMODE3, /*!< 1.5g full-scale. 88 Hz 1st order low pass filter */
  OPMODE4, /*!< 1.5g full-scale. 10 Hz 1st order low pass filter */
};

enum operationMode opMode;
uint16_t sensivity;

uint8_t checkChipID();
uint8_t sca3300_init();
//uint8_t checkCRCFrame(uint8_t *ptr, const uint8_t octets);
uint8_t checkRS( const uint16_t aRsCode );
sca3300Frame sendRequest(uint32_t aRequest);
uint8_t getTemperature(uint16_t *temp);
uint16_t convertTemperature( const uint16_t rawTemp );
uint8_t sca3300_GetAccel( const enum accelAxe aAxe, float *aAccel );
float processAccel( const uint16_t aAccel, const int aSensivity );
uint8_t changeMode( const enum operationMode mode);
uint8_t calculateCRC(uint32_t Data);
static uint8_t bit8_CRC(uint8_t bitValue, uint8_t crc);
uint8_t checkCRCFrame(uint32_t responseData);

#endif /* SRC_SCA3300_SCA3300_H_ */
